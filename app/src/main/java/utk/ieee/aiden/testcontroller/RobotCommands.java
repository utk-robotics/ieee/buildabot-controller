package utk.ieee.aiden.testcontroller;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;


public class RobotCommands {

    private interface CommandIds {
        public static final int TOGGLE_LED = 0;
        public static final int M1_FORWARD = 1;
        public static final int M2_FORWARD = 2;
        public static final int M1_BACKWARD = 3;
        public static final int M2_BACKWARD = 4;
    }

    /**
     * Sends a bluetooth command instructing motor 1 to drive backwards
     * @param speed, 0-255
     */
    public boolean driveM1Backward(byte speed) {
        return false;
    }

    public boolean driveM2Backward(byte speed) {
        return false;

    }

    public boolean driveM1Forward(byte speed) {
        return false;

    }

    public boolean driveM1Backwards(byte speed) {
        return false;

    }

    public boolean setDeviceName() {
        return false;

    }

    public boolean setDevicePIN() {
        return false;

    }

    public boolean toggleLed() {
        return false;

    }
}
