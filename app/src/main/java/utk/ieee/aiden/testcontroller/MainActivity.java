package utk.ieee.aiden.testcontroller;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import io.github.controlwear.virtual.joystick.android.JoystickView;
import me.aflak.bluetooth.Bluetooth;

// import JoystickView;
public class MainActivity extends AppCompatActivity {

    private JoystickView joystick;
    private TextView angleTextView;
    private TextView powerTextView;
    private TextView directionTextView;

    private ImageButton btToggle;
    private ImageButton settings;
    private ImageButton ledToggle;
    private ImageButton stop;

    private BluetoothAdapter btAdapter;
    private BluetoothDevice btDevice;

    int REQUEST_ENABLE_BT = 0;

    private void startIntent(Class c) {
        Intent intent = new Intent(this, c);
        startActivity(intent);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // setup Settings button
        settings = findViewById(R.id.settings_button);
        btToggle = findViewById(R.id.bt_button);
        // setup variable angle and power views.
        angleTextView = findViewById(R.id.textView_angle);
        powerTextView = findViewById(R.id.textView_strength);
        JoystickView joystick = findViewById(R.id.joystickView);
        // bluetooth
        joystick.setOnMoveListener(new JoystickView.OnMoveListener() {
            @Override
            public void onMove(int angle, int strength) {
                angleTextView.setText(angle + "°");
                powerTextView.setText(strength + "%");
            }
        });

        // listener for when selected BT device is changed.

        // bluetooth button listener
        btToggle.setOnTouchListener(new ImageButton.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                startIntent(DeviceListActivity.class);
                return true;
            }
        });

        settings.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                startIntent(SettingsActivity.class);
                return true;
            }
        });

        btAdapter = BluetoothAdapter.getDefaultAdapter();
        if(btAdapter == null) {
            finish();
        }
        if (!btAdapter.isEnabled())
        {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }
    }
}
